module rally_switch(
    buttons=[3,3],
    bar_diameter=28,
    bottom_base_height=3,
    button_bottom_access=12,
    button_diameter=10,
    button_edge_spacing=[8,13],
    button_lock_rim_height=2,
    button_spacing=[5,5],
    cut=1,
    joystick_diameter=15,
    joystick_wall_thickness=3,
    mounting_screw_diameter=4,
    mounting_screw_head_depth=5,
    mounting_screw_head_diameter=6,
    switch_cable_hole_depth=3,
    switch_cable_hole_diameter=16,
    switch_cable_hole_width=6,
    switch_height=15,
    switch_screw_depth=4,
    switch_screw_diameter=3,
    switch_screw_distance_from_edge=3,
    switch_screw_distance_from_top=10,
    switch_screw_spacing=20,
    top_base_height=5,
    show_bottom=true,
    show_top=true,
    )
{
    topHeight = bar_diameter/2 + top_base_height;
    topSizeX = (2 * button_edge_spacing[0]) + (buttons[0] *button_bottom_access) + ((buttons[0] - 1) * button_spacing[0]);
    topSizeY = (2 * button_edge_spacing[1]) + (buttons[1]*button_bottom_access) + ((buttons[1] - 1) * button_spacing[1]);
    
    bottomHeight = bar_diameter/2 + bottom_base_height;
    
    if(show_top) {
        difference() {
            translate([0,0,cut/2]){
                cube([topSizeX, topSizeY, topHeight-cut/2]);
            }
            bar(bar_diameter, topSizeX, topSizeY);
        
            btns(buttons, button_bottom_access, button_edge_spacing, button_spacing, topHeight, button_lock_rim_height, button_diameter);
            switch_cable_hole(switch_cable_hole_depth, switch_cable_hole_diameter, switch_cable_hole_diameter, switch_cable_hole_width, topHeight, switch_screw_distance_from_top, switch_screw_distance_from_edge, switch_screw_spacing,topSizeX,topSizeY);
        
            switch_screw(switch_screw_depth, switch_screw_diameter, switch_screw_distance_from_edge, switch_screw_distance_from_top, switch_screw_spacing,topHeight,topSizeY,topSizeX);
        
            mountHeight = topHeight-top_base_height;
            mounting_screw(topHeight, top_base_height,mounting_screw_diameter, mounting_screw_head_depth, mounting_screw_head_diameter, button_bottom_access, button_edge_spacing,button_diameter,button_spacing,switch_screw_depth,topSizeX, topSizeY,mountHeight, mounting_screw_diameter);
            }
    }
    
    if(show_bottom) {
        translate([0,0,-bottomHeight]) {
        difference() {
            cube([topSizeX, topSizeY, bottomHeight-cut/2]);
            translate([0,0,bottomHeight])
                bar(bar_diameter, topSizeX, topSizeY);
            mounting_screw(topHeight, top_base_height,mounting_screw_diameter, mounting_screw_head_depth, mounting_screw_head_diameter, button_bottom_access, button_edge_spacing,button_diameter,button_spacing,switch_screw_depth, topSizeX, topSizeY, bottomHeight, mounting_screw_diameter);
        
            mounting_screw(topHeight, top_base_height,mounting_screw_diameter, mounting_screw_head_depth, mounting_screw_head_diameter, button_bottom_access, button_edge_spacing,button_diameter,button_spacing,switch_screw_depth, topSizeX, topSizeY, mounting_screw_head_depth, mounting_screw_head_diameter);
        }
    
        sizeXSwitch = button_edge_spacing[0] + 2 * (button_bottom_access + button_spacing[0]) - ((button_bottom_access - button_diameter)/2) - button_spacing[0];
    
        switch(bottomHeight, topSizeX, topSizeY, switch_height, switch_screw_depth, switch_screw_diameter, switch_screw_distance_from_edge, switch_screw_distance_from_top, switch_screw_spacing, joystick_diameter, joystick_wall_thickness, sizeXSwitch, cut);
        }
    }
}


module switch(bottomHeight, topSizeX, topSizeY, switch_height, switch_screw_depth, switch_screw_diameter,    switch_screw_distance_from_edge, switch_screw_distance_from_top, switch_screw_spacing, joystick_diameter,    joystick_wall_thickness, sizeX = 20, cut) {
    
    sizeY = switch_height + joystick_diameter + 2*joystick_wall_thickness;
    outDia = (joystick_diameter + 2*joystick_wall_thickness);
    
    difference() {
        union() {
            translate([topSizeX-sizeX,topSizeY,0]) {
                cube([sizeX, sizeY, (bottomHeight-cut/2)/2]);
            }
            
            translate([topSizeX-sizeX, topSizeY+sizeY-outDia, 0]) {
                cube([sizeX, outDia, bottomHeight]);
            }
            
            translate([topSizeX-sizeX, topSizeY+sizeY-outDia/2, bottomHeight]) {
                  rotate([0,90,0]) {
                    cylinder(h=sizeX, d1=outDia, d2=outDia);
                  }
            }
        }
        
        translate([topSizeX-sizeX-1, topSizeY+sizeY-outDia/2, bottomHeight]) {
            rotate([0,90,0]) {
                cylinder(h=sizeX+2, d1=joystick_diameter, d2=joystick_diameter);
            }
        }
    }   
}


module mounting_screw(topHeight, top_base_height,mounting_screw_diameter, mounting_screw_head_depth, mounting_screw_head_diameter, button_bottom_access, button_edge_spacing,button_diameter,button_spacing,switch_screw_depth, topSizeX, topSizeY, depth, diameter) {
    
    xTrans = button_bottom_access/2+button_edge_spacing[0]+button_diameter/2+button_spacing[0]/2;    
    yTrans = button_edge_spacing[1]/4 + mounting_screw_head_diameter/2;
    xTransArr = [xTrans, topSizeX-xTrans];
    yTransArr = [yTrans, topSizeY-yTrans];
    
    for (xTr=[0:1]) {
        for (yTr=[0:1]){
            translate([xTransArr[xTr],yTransArr[yTr],0])
                cylinder(h=depth, d1=diameter, d2=diameter);
        }
    }
}

module switch_screw(switch_screw_depth, switch_screw_diameter, switch_screw_distance_from_edge, switch_screw_distance_from_top, switch_screw_spacing, topHeight, topSizeY,topSizeX) {
    
    xTransArr = [0, switch_screw_spacing];
    yTransArr = [0, topSizeY-switch_screw_depth];
    
    for (xTr=[0:1]) {
        for (yTr=[0:1]){
            translate([ topSizeX-(switch_screw_distance_from_edge+xTransArr[xTr]), 
                        switch_screw_depth+yTransArr[yTr], 
                        topHeight-switch_screw_distance_from_top]){
                rotate([90,0,0])
                    cylinder(h=switch_screw_depth, d1=switch_screw_diameter, d2=switch_screw_diameter);
            }
        }
    }
}

module switch_cable_hole(switch_cable_hole_depth, switch_cable_hole_diameter, switch_cable_hole_diameter, switch_cable_hole_width, topHeight, switch_screw_distance_from_top, switch_screw_distance_from_edge, switch_screw_spacing,topSizeX,topSizeY) {
    
    y = topSizeY-switch_cable_hole_depth;
    
    for(a=[0:1]) {
        zTrans = topHeight-switch_screw_distance_from_top;
        xTrans =  switch_screw_distance_from_edge+(switch_screw_spacing/2);
    
        length = topSizeX-xTrans;
        translate([0,a*y,zTrans+switch_cable_hole_diameter/2-switch_cable_hole_width])
            cube([length, switch_cable_hole_depth, switch_cable_hole_width]);
        
        translate([topSizeX-xTrans,a*y,zTrans]){
            rotate([0,90,90]) 
                cylinder(h=switch_cable_hole_depth, d1=switch_cable_hole_diameter, d2=switch_cable_hole_diameter);
        }
    }
} 

module btns(buttons, button_bottom_access, button_edge_spacing, button_spacing, topHeight, button_lock_rim_height, button_diameter) {
    for (x = [0:buttons[0]-1]) {
        for (y = [0:buttons[1]-1]) {
            xTrans = button_bottom_access/2+button_edge_spacing[0] + x*(button_spacing[0]+button_bottom_access);
            yTrans = button_bottom_access/2+button_edge_spacing[1] + y*(button_spacing[1]+button_bottom_access);
            
            translate([xTrans, yTrans, topHeight-button_lock_rim_height]) {
                cylinder(h=button_lock_rim_height, d1=button_diameter, d2=button_diameter);
            }
            
            translate([xTrans, yTrans, 0]) {
                cylinder(h=topHeight-button_lock_rim_height, d1=button_bottom_access, d2=button_bottom_access);
            }
        }
    }
}

module bar(bar_diameter, topSizeX, topSizeY) {
    translate([-1,topSizeY/2,0]){
        rotate([0,90,0]) {
            cylinder(h=topSizeX+2, d1=bar_diameter, d2=bar_diameter);
        }
    }
}


rally_switch();
